<?php

/**
 * @file
 * uw_ct_feds_footer.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_footer_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_footer';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds Footer';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Feds Footer';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  /* Field: Content: Facebook */
  $handler->display->display_options['fields']['field_feds_facebook']['id'] = 'field_feds_facebook';
  $handler->display->display_options['fields']['field_feds_facebook']['table'] = 'field_data_field_feds_facebook';
  $handler->display->display_options['fields']['field_feds_facebook']['field'] = 'field_feds_facebook';
  /* Field: Content: Instagram */
  $handler->display->display_options['fields']['field_feds_instagram']['id'] = 'field_feds_instagram';
  $handler->display->display_options['fields']['field_feds_instagram']['table'] = 'field_data_field_feds_instagram';
  $handler->display->display_options['fields']['field_feds_instagram']['field'] = 'field_feds_instagram';
  /* Field: Content: LinkedIn */
  $handler->display->display_options['fields']['field_linkedin']['id'] = 'field_linkedin';
  $handler->display->display_options['fields']['field_linkedin']['table'] = 'field_data_field_linkedin';
  $handler->display->display_options['fields']['field_linkedin']['field'] = 'field_linkedin';
  /* Field: Content: Twitter */
  $handler->display->display_options['fields']['field_feds_twitter']['id'] = 'field_feds_twitter';
  $handler->display->display_options['fields']['field_feds_twitter']['table'] = 'field_data_field_feds_twitter';
  $handler->display->display_options['fields']['field_feds_twitter']['field'] = 'field_feds_twitter';
  /* Field: Content: YouTube */
  $handler->display->display_options['fields']['field_feds_youtube']['id'] = 'field_feds_youtube';
  $handler->display->display_options['fields']['field_feds_youtube']['table'] = 'field_data_field_feds_youtube';
  $handler->display->display_options['fields']['field_feds_youtube']['field'] = 'field_feds_youtube';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_footer' => 'feds_footer',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'full';
  $handler->display->display_options['defaults']['row_options'] = FALSE;

  /* Display: Footer Column 1 */
  $handler = $view->new_display('block', 'Footer Column 1', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title_field']['id'] = 'title_field';
  $handler->display->display_options['fields']['title_field']['table'] = 'field_data_title_field';
  $handler->display->display_options['fields']['title_field']['field'] = 'title_field';
  $handler->display->display_options['fields']['title_field']['label'] = '';
  $handler->display->display_options['fields']['title_field']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title_field']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title_field']['link_to_entity'] = 0;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Facebook */
  $handler->display->display_options['fields']['field_feds_facebook']['id'] = 'field_feds_facebook';
  $handler->display->display_options['fields']['field_feds_facebook']['table'] = 'field_data_field_feds_facebook';
  $handler->display->display_options['fields']['field_feds_facebook']['field'] = 'field_feds_facebook';
  $handler->display->display_options['fields']['field_feds_facebook']['label'] = '';
  $handler->display->display_options['fields']['field_feds_facebook']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_feds_facebook']['alter']['text'] = '<a href="[field_feds_facebook-url]">
<i class="fa fa-facebook"></i>
<span class="off-screen">[field_feds_facebook-title]</span>
</a>';
  $handler->display->display_options['fields']['field_feds_facebook']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_feds_facebook']['element_wrapper_class'] = 'feds-footer-social';
  $handler->display->display_options['fields']['field_feds_facebook']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_feds_facebook']['click_sort_column'] = 'url';
  /* Field: Content: Twitter */
  $handler->display->display_options['fields']['field_feds_twitter']['id'] = 'field_feds_twitter';
  $handler->display->display_options['fields']['field_feds_twitter']['table'] = 'field_data_field_feds_twitter';
  $handler->display->display_options['fields']['field_feds_twitter']['field'] = 'field_feds_twitter';
  $handler->display->display_options['fields']['field_feds_twitter']['label'] = '';
  $handler->display->display_options['fields']['field_feds_twitter']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_feds_twitter']['alter']['text'] = '<a href="[field_feds_twitter-url]">
<i class="fa fa-twitter"></i>
<span class="off-screen">[field_feds_twitter-title]</span>
</a>';
  $handler->display->display_options['fields']['field_feds_twitter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_feds_twitter']['element_wrapper_class'] = 'feds-footer-social';
  $handler->display->display_options['fields']['field_feds_twitter']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_feds_twitter']['click_sort_column'] = 'url';
  /* Field: Content: Instagram */
  $handler->display->display_options['fields']['field_feds_instagram']['id'] = 'field_feds_instagram';
  $handler->display->display_options['fields']['field_feds_instagram']['table'] = 'field_data_field_feds_instagram';
  $handler->display->display_options['fields']['field_feds_instagram']['field'] = 'field_feds_instagram';
  $handler->display->display_options['fields']['field_feds_instagram']['label'] = '';
  $handler->display->display_options['fields']['field_feds_instagram']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_feds_instagram']['alter']['text'] = '<a href="[field_feds_instagram-url]">
<i class="fa fa-instagram"></i>
<span class="off-screen">[field_feds_instagram-title]</span>
</a>';
  $handler->display->display_options['fields']['field_feds_instagram']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_feds_instagram']['element_wrapper_class'] = 'feds-footer-social';
  $handler->display->display_options['fields']['field_feds_instagram']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_feds_instagram']['click_sort_column'] = 'url';
  /* Field: Content: YouTube */
  $handler->display->display_options['fields']['field_feds_youtube']['id'] = 'field_feds_youtube';
  $handler->display->display_options['fields']['field_feds_youtube']['table'] = 'field_data_field_feds_youtube';
  $handler->display->display_options['fields']['field_feds_youtube']['field'] = 'field_feds_youtube';
  $handler->display->display_options['fields']['field_feds_youtube']['label'] = '';
  $handler->display->display_options['fields']['field_feds_youtube']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_feds_youtube']['alter']['text'] = '<a href="[field_feds_youtube-url]">
<i class="fa fa-youtube"></i>
<span class="off-screen">[field_feds_youtube-title]</span>
</a>';
  $handler->display->display_options['fields']['field_feds_youtube']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_feds_youtube']['element_wrapper_class'] = 'feds-footer-social';
  $handler->display->display_options['fields']['field_feds_youtube']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_feds_youtube']['click_sort_column'] = 'url';
  /* Field: Content: LinkedIn */
  $handler->display->display_options['fields']['field_linkedin']['id'] = 'field_linkedin';
  $handler->display->display_options['fields']['field_linkedin']['table'] = 'field_data_field_linkedin';
  $handler->display->display_options['fields']['field_linkedin']['field'] = 'field_linkedin';
  $handler->display->display_options['fields']['field_linkedin']['label'] = '';
  $handler->display->display_options['fields']['field_linkedin']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_linkedin']['alter']['text'] = '<a href="[field_linkedin-url]">
<i class="fa fa-linkedin"></i>
<span class="off-screen">[field_linkedin-title]</span>
</a>';
  $handler->display->display_options['fields']['field_linkedin']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_linkedin']['element_wrapper_class'] = 'feds-footer-social';
  $handler->display->display_options['fields']['field_linkedin']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_linkedin']['click_sort_column'] = 'url';
  $handler->display->display_options['block_description'] = 'Feds Footer Column 1';

  /* Display: Footer Column 2 */
  $handler = $view->new_display('block', 'Footer Column 2', 'block_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title Column 2 */
  $handler->display->display_options['fields']['field_title_column_2_1']['id'] = 'field_title_column_2_1';
  $handler->display->display_options['fields']['field_title_column_2_1']['table'] = 'field_data_field_title_column_2';
  $handler->display->display_options['fields']['field_title_column_2_1']['field'] = 'field_title_column_2';
  $handler->display->display_options['fields']['field_title_column_2_1']['label'] = '';
  $handler->display->display_options['fields']['field_title_column_2_1']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_title_column_2_1']['element_label_colon'] = FALSE;
  /* Field: Content: Content Column 2 */
  $handler->display->display_options['fields']['field_footer_content_column_2']['id'] = 'field_footer_content_column_2';
  $handler->display->display_options['fields']['field_footer_content_column_2']['table'] = 'field_data_field_footer_content_column_2';
  $handler->display->display_options['fields']['field_footer_content_column_2']['field'] = 'field_footer_content_column_2';
  $handler->display->display_options['fields']['field_footer_content_column_2']['label'] = '';
  $handler->display->display_options['fields']['field_footer_content_column_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['block_description'] = 'Feds Footer Column 2';

  /* Display: Footer Column 3 */
  $handler = $view->new_display('block', 'Footer Column 3', 'block_4');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title Column 3 */
  $handler->display->display_options['fields']['field_title_column_3']['id'] = 'field_title_column_3';
  $handler->display->display_options['fields']['field_title_column_3']['table'] = 'field_data_field_title_column_3';
  $handler->display->display_options['fields']['field_title_column_3']['field'] = 'field_title_column_3';
  $handler->display->display_options['fields']['field_title_column_3']['label'] = '';
  $handler->display->display_options['fields']['field_title_column_3']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_title_column_3']['element_label_colon'] = FALSE;
  /* Field: Content: Content Column 3 */
  $handler->display->display_options['fields']['field_content_column_3']['id'] = 'field_content_column_3';
  $handler->display->display_options['fields']['field_content_column_3']['table'] = 'field_data_field_content_column_3';
  $handler->display->display_options['fields']['field_content_column_3']['field'] = 'field_content_column_3';
  $handler->display->display_options['fields']['field_content_column_3']['label'] = '';
  $handler->display->display_options['fields']['field_content_column_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['block_description'] = 'Feds Footer Column 3';

  /* Display: Social Media Links */
  $handler = $view->new_display('block', 'Social Media Links', 'social_media_links');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'feds-sm-header';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_feds_facebook' => 'field_feds_facebook',
    'field_feds_instagram' => 'field_feds_instagram',
    'field_linkedin' => 'field_linkedin',
    'field_feds_twitter' => 'field_feds_twitter',
    'field_feds_youtube' => 'field_feds_youtube',
  );
  $handler->display->display_options['row_options']['separator'] = '</li><li>';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Facebook */
  $handler->display->display_options['fields']['field_feds_facebook']['id'] = 'field_feds_facebook';
  $handler->display->display_options['fields']['field_feds_facebook']['table'] = 'field_data_field_feds_facebook';
  $handler->display->display_options['fields']['field_feds_facebook']['field'] = 'field_feds_facebook';
  $handler->display->display_options['fields']['field_feds_facebook']['label'] = '';
  $handler->display->display_options['fields']['field_feds_facebook']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_feds_facebook']['alter']['text'] = '<a href="[field_feds_facebook-url]">
   <i class="fa fa-facebook"></i>
   <span class="off-screen">[field_feds_facebook-title]</span>
</a>';
  $handler->display->display_options['fields']['field_feds_facebook']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_feds_facebook']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_feds_facebook']['click_sort_column'] = 'url';
  /* Field: Content: Twitter */
  $handler->display->display_options['fields']['field_feds_twitter']['id'] = 'field_feds_twitter';
  $handler->display->display_options['fields']['field_feds_twitter']['table'] = 'field_data_field_feds_twitter';
  $handler->display->display_options['fields']['field_feds_twitter']['field'] = 'field_feds_twitter';
  $handler->display->display_options['fields']['field_feds_twitter']['label'] = '';
  $handler->display->display_options['fields']['field_feds_twitter']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_feds_twitter']['alter']['text'] = '<a href="[field_feds_twitter-url]">
   <i class="fa fa-twitter"></i>
   <span class="off-screen">[field_feds_twitter-title]</span>
</a>';
  $handler->display->display_options['fields']['field_feds_twitter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_feds_twitter']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_feds_twitter']['click_sort_column'] = 'url';
  /* Field: Content: Instagram */
  $handler->display->display_options['fields']['field_feds_instagram']['id'] = 'field_feds_instagram';
  $handler->display->display_options['fields']['field_feds_instagram']['table'] = 'field_data_field_feds_instagram';
  $handler->display->display_options['fields']['field_feds_instagram']['field'] = 'field_feds_instagram';
  $handler->display->display_options['fields']['field_feds_instagram']['label'] = '';
  $handler->display->display_options['fields']['field_feds_instagram']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_feds_instagram']['alter']['text'] = '<a href="[field_feds_instagram-url]">
   <i class="fa fa-instagram"></i>
   <span class="off-screen">[field_feds_instagram-title]</span>
</a>';
  $handler->display->display_options['fields']['field_feds_instagram']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_feds_instagram']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_feds_instagram']['click_sort_column'] = 'url';
  /* Field: Content: YouTube */
  $handler->display->display_options['fields']['field_feds_youtube']['id'] = 'field_feds_youtube';
  $handler->display->display_options['fields']['field_feds_youtube']['table'] = 'field_data_field_feds_youtube';
  $handler->display->display_options['fields']['field_feds_youtube']['field'] = 'field_feds_youtube';
  $handler->display->display_options['fields']['field_feds_youtube']['label'] = '';
  $handler->display->display_options['fields']['field_feds_youtube']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_feds_youtube']['alter']['text'] = '<a href="[field_feds_youtube-url]">
   <i class="fa fa-youtube"></i>
   <span class="off-screen">[field_feds_youtube-title]</span>
</a>';
  $handler->display->display_options['fields']['field_feds_youtube']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_feds_youtube']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_feds_youtube']['click_sort_column'] = 'url';
  /* Field: Content: LinkedIn */
  $handler->display->display_options['fields']['field_linkedin']['id'] = 'field_linkedin';
  $handler->display->display_options['fields']['field_linkedin']['table'] = 'field_data_field_linkedin';
  $handler->display->display_options['fields']['field_linkedin']['field'] = 'field_linkedin';
  $handler->display->display_options['fields']['field_linkedin']['label'] = '';
  $handler->display->display_options['fields']['field_linkedin']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_linkedin']['alter']['text'] = '<a href="[field_linkedin-url]">
   <i class="fa fa-linkedin"></i>
   <span class="off-screen">[field_linkedin-title]</span>
</a>';
  $handler->display->display_options['fields']['field_linkedin']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_linkedin']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_linkedin']['click_sort_column'] = 'url';
  $translatables['feds_footer'] = array(
    t('Master'),
    t('Feds Footer'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Facebook'),
    t('Instagram'),
    t('LinkedIn'),
    t('Twitter'),
    t('YouTube'),
    t('Block'),
    t('Footer Column 1'),
    t('<a href="[field_feds_facebook-url]">
<i class="fa fa-facebook"></i>
<span class="off-screen">[field_feds_facebook-title]</span>
</a>'),
    t('<a href="[field_feds_twitter-url]">
<i class="fa fa-twitter"></i>
<span class="off-screen">[field_feds_twitter-title]</span>
</a>'),
    t('<a href="[field_feds_instagram-url]">
<i class="fa fa-instagram"></i>
<span class="off-screen">[field_feds_instagram-title]</span>
</a>'),
    t('<a href="[field_feds_youtube-url]">
<i class="fa fa-youtube"></i>
<span class="off-screen">[field_feds_youtube-title]</span>
</a>'),
    t('<a href="[field_linkedin-url]">
<i class="fa fa-linkedin"></i>
<span class="off-screen">[field_linkedin-title]</span>
</a>'),
    t('Feds Footer Column 1'),
    t('Footer Column 2'),
    t('Feds Footer Column 2'),
    t('Footer Column 3'),
    t('Feds Footer Column 3'),
    t('Social Media Links'),
    t('<a href="[field_feds_facebook-url]">
   <i class="fa fa-facebook"></i>
   <span class="off-screen">[field_feds_facebook-title]</span>
</a>'),
    t('<a href="[field_feds_twitter-url]">
   <i class="fa fa-twitter"></i>
   <span class="off-screen">[field_feds_twitter-title]</span>
</a>'),
    t('<a href="[field_feds_instagram-url]">
   <i class="fa fa-instagram"></i>
   <span class="off-screen">[field_feds_instagram-title]</span>
</a>'),
    t('<a href="[field_feds_youtube-url]">
   <i class="fa fa-youtube"></i>
   <span class="off-screen">[field_feds_youtube-title]</span>
</a>'),
    t('<a href="[field_linkedin-url]">
   <i class="fa fa-linkedin"></i>
   <span class="off-screen">[field_linkedin-title]</span>
</a>'),
  );
  $export['feds_footer'] = $view;

  return $export;
}
