<?php

/**
 * @file
 * uw_ct_feds_footer.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_footer_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_footer content'.
  $permissions['create feds_footer content'] = array(
    'name' => 'create feds_footer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_footer content'.
  $permissions['delete any feds_footer content'] = array(
    'name' => 'delete any feds_footer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_footer content'.
  $permissions['delete own feds_footer content'] = array(
    'name' => 'delete own feds_footer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any feds_footer content'.
  $permissions['edit any feds_footer content'] = array(
    'name' => 'edit any feds_footer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_footer content'.
  $permissions['edit own feds_footer content'] = array(
    'name' => 'edit own feds_footer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_footer revision log entry'.
  $permissions['enter feds_footer revision log entry'] = array(
    'name' => 'enter feds_footer revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_footer authored by option'.
  $permissions['override feds_footer authored by option'] = array(
    'name' => 'override feds_footer authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_footer authored on option'.
  $permissions['override feds_footer authored on option'] = array(
    'name' => 'override feds_footer authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_footer promote to front page option'.
  $permissions['override feds_footer promote to front page option'] = array(
    'name' => 'override feds_footer promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_footer published option'.
  $permissions['override feds_footer published option'] = array(
    'name' => 'override feds_footer published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_footer revision option'.
  $permissions['override feds_footer revision option'] = array(
    'name' => 'override feds_footer revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_footer sticky option'.
  $permissions['override feds_footer sticky option'] = array(
    'name' => 'override feds_footer sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_footer content'.
  $permissions['search feds_footer content'] = array(
    'name' => 'search feds_footer content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search uw_site_footer content'.
  $permissions['search uw_site_footer content'] = array(
    'name' => 'search uw_site_footer content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
