<?php

/**
 * @file
 * uw_ct_feds_footer.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_feds_footer_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_footer_column_1|node|feds_footer|default';
  $field_group->group_name = 'group_feds_footer_column_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_footer';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Footer Column 1',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'title_field',
      2 => 'group_feds_social_media_links',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Footer Column 1',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'footer-column',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
        'required_fields' => 1,
      ),
      'formatter' => '',
    ),
  );
  $field_groups['group_feds_footer_column_1|node|feds_footer|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_footer_column_1|node|feds_footer|form';
  $field_group->group_name = 'group_feds_footer_column_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_footer';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Footer Column 1',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'title_field',
      2 => 'group_feds_social_media_links',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Footer Column 1',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-feds-footer-column-1 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_feds_footer_column_1|node|feds_footer|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_footer_column_2|node|feds_footer|default';
  $field_group->group_name = 'group_feds_footer_column_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_footer';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Footer Column 2',
    'weight' => '1',
    'children' => array(
      0 => 'field_footer_content_column_2',
      1 => 'field_title_column_2',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Footer Column 2',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'footer-column',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
        'required_fields' => 1,
      ),
      'formatter' => '',
    ),
  );
  $field_groups['group_feds_footer_column_2|node|feds_footer|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_footer_column_2|node|feds_footer|form';
  $field_group->group_name = 'group_feds_footer_column_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_footer';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Footer Column 2',
    'weight' => '1',
    'children' => array(
      0 => 'field_footer_content_column_2',
      1 => 'field_title_column_2',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-feds-footer-column-2 field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_feds_footer_column_2|node|feds_footer|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_footer_column_3|node|feds_footer|default';
  $field_group->group_name = 'group_feds_footer_column_3';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_footer';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Footer Column 3',
    'weight' => '2',
    'children' => array(
      0 => 'field_title_column_3',
      1 => 'field_content_column_3',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Footer Column 3',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'footer-column',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
        'required_fields' => 1,
      ),
      'formatter' => '',
    ),
  );
  $field_groups['group_feds_footer_column_3|node|feds_footer|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_footer_column_3|node|feds_footer|form';
  $field_group->group_name = 'group_feds_footer_column_3';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_footer';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Footer Column 3',
    'weight' => '2',
    'children' => array(
      0 => 'field_title_column_3',
      1 => 'field_content_column_3',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-feds-footer-column-3 field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_feds_footer_column_3|node|feds_footer|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_social_media_links|node|feds_footer|default';
  $field_group->group_name = 'group_feds_social_media_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_footer';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_feds_footer_column_1';
  $field_group->data = array(
    'label' => 'Feds Social Media Links',
    'weight' => '3',
    'children' => array(
      0 => 'field_feds_facebook',
      1 => 'field_feds_twitter',
      2 => 'field_feds_youtube',
      3 => 'field_feds_instagram',
      4 => 'field_linkedin',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Feds Social Media Links',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'socialmedia',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_feds_social_media_links|node|feds_footer|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feds_social_media_links|node|feds_footer|form';
  $field_group->group_name = 'group_feds_social_media_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_footer';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_feds_footer_column_1';
  $field_group->data = array(
    'label' => 'Social Media Links',
    'weight' => '3',
    'children' => array(
      0 => 'field_feds_facebook',
      1 => 'field_feds_twitter',
      2 => 'field_feds_youtube',
      3 => 'field_feds_instagram',
      4 => 'field_linkedin',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-feds-social-media-links field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_feds_social_media_links|node|feds_footer|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds Social Media Links');
  t('Footer Column 1');
  t('Footer Column 2');
  t('Footer Column 3');
  t('Social Media Links');

  return $field_groups;
}
