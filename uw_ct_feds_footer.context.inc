<?php

/**
 * @file
 * uw_ct_feds_footer.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_footer_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_footer';
  $context->description = 'Feds footer view';
  $context->tag = 'Feds';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_nav_site_footer-site-footer' => array(
          'module' => 'uw_nav_site_footer',
          'delta' => 'site-footer',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'views-feds_footer-social_media_links' => array(
          'module' => 'views',
          'delta' => 'feds_footer-social_media_links',
          'region' => 'feds_social_media',
          'weight' => '-10',
        ),
        'views-feds_footer-block_1' => array(
          'module' => 'views',
          'delta' => 'feds_footer-block_1',
          'region' => 'footer_content_1',
          'weight' => '-10',
        ),
        'views-feds_footer-block_2' => array(
          'module' => 'views',
          'delta' => 'feds_footer-block_2',
          'region' => 'footer_content_2',
          'weight' => '-10',
        ),
        'views-feds_footer-block_4' => array(
          'module' => 'views',
          'delta' => 'feds_footer-block_4',
          'region' => 'footer_content_3',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Feds footer view');
  $export['feds_footer'] = $context;

  return $export;
}
