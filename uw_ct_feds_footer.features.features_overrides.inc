<?php

/**
 * @file
 * uw_ct_feds_footer.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_feds_footer_features_override_default_overrides() {
  // This code is only used for UI in features. Export alter hooks do the magic.
  $overrides = array();

  // Exported overrides for: context.
  $overrides["context.global_footer.reactions|block|blocks|uw_nav_global_footer-footer-1"]["DELETED"] = TRUE;
  $overrides["context.global_footer.reactions|block|blocks|uw_nav_global_footer-footer-2"]["DELETED"] = TRUE;
  $overrides["context.global_footer.reactions|block|blocks|uw_nav_global_footer-footer-3"]["DELETED"] = TRUE;
  $overrides["context.global_footer.reactions|block|blocks|uw_nav_global_footer-social"]["DELETED"] = TRUE;

  return $overrides;
}
